# v0.0.0.9005

## Add
- obj functions
- arg pad_col to vox2las

## Change
- __upgrade to lidR3__

## Fix
- bug in plot3D.LAS
- dependency for las2gif
- offset adjustment and quantization after rotation
- bbox in lascenter

See commits for other changes


# v0.0.0.9004

## Add

- las2gif: function to make a GIF from LAS object
- lascenter: center LAS coordinates to 0,0,0
- lasrotate: apply affine transformation to point cloud with 4D matrix

# v0.0.0.9003

## Add

- AMAPVox wrapper

## Change

- voxelSD.read becomes read_vox for both voxelSD and AMAPVox

# v0.0.0.9002

Adapt to package to lidR2:

- changed LAS slot crs to proj4string
- removed processing by reference (lasenergy, renumber)


# v0.0.0.9001

#### NEW FEATURES

* function `write_las4vox` replaced by `las2sht` and `write_sht` and upgraded to output AMAPVox sht files
* `invert_traj` : invert trajectory from point cloud
