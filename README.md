# lidRtools: LIDAR processing tools for lidR

# Install

Install `lidRtools` from an RStudio session:

```
devtools::install_git("https://gitlab.com/floriandeboissieu/lidRtools.git")
```

# Features
- Read and add trajectography to LAS object
- Add zenith and azimuth angles of incident pulses
- Update LAS Return Numbers and Number Of Returns
- Ray-tracing voxelisation
- 2D and 3D plot functions for LAS, VOX and PADP
- Isolate pulses having echos inside a LAS
- Normalize to DTM with DTM interpollation (deprecated, use lidR::lasnormalise)

# Citation

If you use __lidRtools__, please cite the following reference:

Florian de Boissieu, 2019, lidRtools: R package of LIDAR data processing tools, in complement of lidR package. https://gitlab.com/floriandeboissieu/lidRtools.
