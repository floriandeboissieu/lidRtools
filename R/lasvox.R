# ===============================================================================
# PROGRAMMERS:
#
# Florian de Boissieu <fdeboiss@gmail.com>  -  https://gitlab.com/floriandeboissieu/lidRtools
#
# Copyright 2017 Florian de Boissieu
#
# This file is part of lidRtools R package.
#
# lidRtools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ===============================================================================

#' Compute ray tracing PAD of ALS point cloud.
#'
#' Compute ray tracing PAD of ALS point cloud.
#' @param las LAS object.
#' @param Xc,Yc,Zmin coordiantes of the center of the voxelized area. Voxels space coordinates are considered as float, thus
#' las is centered before processing to avoid any float to double numerical problem.
#' @param r,r2 numeric. Similar to \code{\link[lidR]{catalog_queries}}.
#' @param res numeric of length 1 or 3. Voxel resolution, x,y,z if length is 3.
#' @param method character. Either 'AMAPVox' or 'voxelsSD'.
#' Details in \link[AMAPVox]{AMAPVox} and \link[voxelSD]{voxelSD}.
#' @param traj data.table with gpstime, x, y, z. Trajectory points, see \code{\link{read_traj}}.
#' If `NULL`, trajectory is expected in `las@data`, see \link{lastraj}.
#' @return list as returned by \link{read_vox}.
#' @references
#' Grau, E., Durrieu, S., Fournier, R., Gastellu-Etchegorry, J.-P., Yin, T., 2017.
#' Estimation of 3D vegetation density with Terrestrial Laser Scanning data using voxels.
#' A sensitivity analysis of influencing parameters. Remote Sensing of Environment 191, 373–388.
#' https://doi.org/10.1016/j.rse.2017.01.032
#' @export
lasvox <- function(las, Xc, Yc, Zmin, r, r2 = NULL, res = 1, method='AMAPVox', traj = NULL){
  # Voxelisation is made with float coordinates
  # thus it has to be centered before use, otherwise,
  # if coordinates are large (example EPSG:2154), it may fail
  SOPmat = diag(4)
  SOPmat[1:3,4] = -c(Xc, Yc, Zmin)

  if(is.null(r2)){
    rx = r
    ry = r
  }else{
    rx = r/2
    ry = r2/2
  }

  las = lasclipRectangle(las, Xc-rx, Yc-ry, Xc+rx, Yc+ry)
  Zmax = max(las@data$Z)-Zmin

  lim = c(-rx, rx, -ry, ry, 0, Zmax)
  # lim = c(-rx+Xc, rx+Xc, -ry+Yc, ry+Yc, Zmin, Zmax)
  # print(lim)
  if(method == 'AMAPVox'){
    vox = AMAPVox(las, traj = traj,
                  res = res, vop = SOPmat,
                  lim = lim)
  }else if(method == 'voxelSD'){
    vox = voxelSD(las, traj = traj,
                  res = res, SOPmat = SOPmat,
                  lim = lim)
  }else{
    stop('lasvox method not available')
  }
  vox$header$min_corner = vox$header$min_corner+c(Xc, Yc, Zmin)
  return(vox)
}
