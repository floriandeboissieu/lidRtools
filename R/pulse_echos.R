# ===============================================================================
# PROGRAMMERS:
#
# Florian de Boissieu <fdeboiss@gmail.com>  -  https://gitlab.com/floriandeboissieu/lidRtools
#
# Copyright 2017 Florian de Boissieu
#
# This file is part of lidRtools R package.
#
# lidRtools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ===============================================================================

#' Extracts from Catalog the echos of LAS pulses.
#'
#' Returns a LAS object with all echos of pulses having at least one echo in original las.
#' The function depends on the GPS time to retrieve each individual beam.
#'
#' @param .las LAS object. See \link[lidR]{LAS}.
#' @param .lascat Catalog object. See \link[lidR]{catalog}.
#' @param Amax,Hmax numeric. Maximum incident angle in radiants and maximum height. If NULL, values computed are from input LAS data.
#' @return LAS object containing all the echos of beams falling into input LAS extent.
#'
#' @export pulse_echos
pulse_echos <- function(.las, .lascat, Amax = (30*pi/180), Hmax = 60){
  if(is.null(Hmax))
    Hmax <- (.las$`Max Z`[[1]]-.las$`Min Z`[[1]])
  e_las <- lidR::extent(.las)
  buffer <- abs(tan(Amax)*Hmax)

  capture.output(newlas <- lidR::lasfilter(lidR::lasclipRectangle(.lascat, e_las+buffer)[[1]],
                                     gpstime %in% .las$gpstime),file="/dev/null")
  return(newlas)
}
