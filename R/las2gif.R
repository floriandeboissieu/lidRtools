#' Make a gif around LAS
#'
#' Make a gif around LAS
#' @param las LAS object.
#' @param color character. Column name to use for color.
#' @param phi numeric. The angle defining the colatitude viewing direction, see \link[plot3D]{scatter3D}.
#' @param dtheta numeric. The angle defining the step of azimuthal viewing direction
#' between frames, theta going from 0 to 360.
#' @param colkey logical. If TRUE plots the colorbar, see \link[plot3D]{scatter3D}.
#' @param interval numeric. A positive number to set the time interval of the animation (unit in seconds).
#' @param scale numeric or 3 element vector. Scale factor to apply to point cloud.
#' @param file character. GIF file path.
#' @param ... other arguments passed to \link[plot3D]{scatter3D}
#' and \link[animation]{saveGIF}.
#' @import lidR data.table animation
#' @export
las2gif <- function(las, color = 'Z', phi = 20, dtheta = 5, colkey = F,
                    interval = .1, scale = 1, file = 'animation.gif', ...){

  las = lascenter(las)
  if(length(scale)==1)
    scale = rep(scale, 3)

  bbox = c(las@header@PHB$`Min X`*scale[1], las@header@PHB$`Max X`*scale[1],
           las@header@PHB$`Min Y`*scale[2], las@header@PHB$`Max Y`*scale[2],
           las@header@PHB$`Min Z`*scale[3], las@header@PHB$`Max Z`*scale[3])


  saveGIF({
    for(theta in seq(0, 360, dtheta)){
      tr = theta*pi/180
      mat = matrix(c(cos(tr), -sin(tr), 0, 0,
                     sin(tr), cos(tr), 0, 0,
                     0, 0, 1, 0,
                     0, 0, 0, 1), byrow = T, nrow = 4)

      las1 = lasrotate(las, mat)
      mar <- par()$mar
      par(mar=c(0,0,0,0))
      plot3D.LAS(las1, color = color,
                 bty ='n', phi=phi, theta=0, colkey = colkey,
                 xlim=c(bbox[1],bbox[2]), ylim=c(bbox[3],bbox[4]),
                 zlim=c(bbox[5], bbox[6]), ...)
      par(mar = mar)
    }
  }, interval = interval, movie.name = file, ...)
  invisible()
}
