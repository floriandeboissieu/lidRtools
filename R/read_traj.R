# ===============================================================================
# PROGRAMMERS:
#
# Florian de Boissieu <fdeboiss@gmail.com>  -  https://gitlab.com/floriandeboissieu/lidRtools
#
# Copyright 2017 Florian de Boissieu
#
# This file is part of lidRtools R package.
#
# lidRtools is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#
# ===============================================================================
#' Reads trajectory ASCII files of X,Y,Z position and Roll, Pitch, Heading orientation of plane.
#'
#' Reads trajectory ASCII files of X,Y,Z position and Roll, Pitch, Heading (or Yaw) orientation.
#' This function is used for LAS angle computation.
#'
#' @param file character. File or directory, wild cards * can be used (see \code{\link[utils]{glob2rx}})
#' @param cnames character. A single character string with format of file. See details.
#' @param cnum integer. An integer vector designating the column number of each of the variable of cnames.
#'
#' @return A data.table with variables {gpstime, x, y, z, roll, pitch, yaw}
#' @details \code{read_traj} extracts only the column named in \code{cnames} which has to be a single string, each character giving the signification of the column.
#' t=time, position {x, y, z}, and IMU angles r=Roll, p=Pitch, h=Heading or Yaw.
#' Example, to get only time and position, cnames="txyz", to get the time, position and IMU, cnames="txyzrph".
#' @examples
#' \dontrun{
#' traj <- read_traj("traj/*.asc","txyz",c(4,1,2,3))
#' }
#'
#' @seealso \code{\link{lastraj}}
#'
#' @export
read_traj=function(file, cnames="txyz", cnum=c(1:nchar(cnames))){

  formatColnames=c("t","x","y","z","r","p","h")
  outColnames=c("gpstime","x","y","z","roll","pitch","yaw")

  if(length(cnum)!=nchar(cnames))
    error('cnames and cnum have different length.')

  if(dir.exists(file)){ # case file is a directory
    trajDpath=file
    trajFpat=glob2rx("*")
  }else if(dir.exists(dirname(file))){ # case file is a file or a path with pattern
    trajDpath=dirname(file)
    trajFpat=glob2rx(basename(file))
  }

  trajFlist=list.files(trajDpath, full.names = T,pattern = trajFpat)

  if(length(trajFlist)==0)
    {
    warning("Files not found.")
    return(data.table::data.table())
  }

  cnames_v=strsplit(cnames,"")[[1]]
  traj=do.call(rbind,lapply(trajFlist,data.table::fread, select = cnum, col.names = cnames_v)) # load toute la trajectoire
  data.table::setcolorder(traj,formatColnames[formatColnames %in% cnames_v])
  names(traj)=outColnames[formatColnames %in% cnames_v]
  setkey(traj, gpstime)
  return(traj)
}
